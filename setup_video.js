const fs = require('fs')

const diretorio_videos = 'videos'

function setup_video () {

    // para alterar a lista de videos disponiveis quando ficheiros forem adicionados
    // ou removidos do diretorio
    fs.watchFile(diretorio_videos, __do_setup_video)
    __do_setup_video() // efetuar setup inicial

}


/**
 * ver todos os videos disponiveis na pasta 'videos'
 * iniciar a reproducao em loop
 */
function __do_setup_video () {

    var videos_disponiveis = null
    var video_atual        = 0
    var video_player       = null

    video_player = document.getElementById('videos-publicidade-player')
    videos_disponiveis = fs.readdirSync(diretorio_videos)
    if (videos_disponiveis.length === 0) {
        console.warn(`Nenhum video encontrado no diretorio ${diretorio_videos}`)
        video_player.onended = undefined
        video_player.src     = undefined
        return
    }

    // para reproduzir proximo video quando atual terminar
    video_player.onended =  () => {
        var proximo_video = (++video_atual) % videos_disponiveis.length
        video_player.src = `${diretorio_videos}/${videos_disponiveis[proximo_video]}`
        video_player.play()
    }
    // iniciar primeiro video
    video_player.src = `${diretorio_videos}/${videos_disponiveis[0]}`
    video_player.play()

}


module.exports = setup_video
